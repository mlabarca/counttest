﻿using UnityEngine;
using System.Collections;

public class QuestionState : UnitySingleton<QuestionState> {
	// Class with that inherits from singleton pattern that is available for access using
	// QuestionState.Instance ; Allows to do some actions before calling methods registered to events,
	// Such as changing states

	// Are we allowed to answer questions, is the timer running?
	public bool timerOn = false;
		
	public void StartGame (){
		QuestionEventManager.TriggerGameStart();
	}
	
	public void EndGame(){
		QuestionEventManager.TriggerGameOver();
	}

	public void markWrong(){
		QuestionEventManager.TriggerWrong();
	}

	public void markCorrect(){
		QuestionEventManager.TriggerCorrect();
	}

	public void NextQuestion(){
		QuestionEventManager.TriggerNext();
	}



}
