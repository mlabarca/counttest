﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuestionControl : MonoBehaviour {
	// Manager class that will go through a public array of questions and ask each one


	public List<CountQuestion> questions = new List<CountQuestion>();


	// Locations in scene where we will place the question items
	public Transform itemsLocation;
	public Transform firstAnswerLoc;
	public float answerSpacing;

	// Amount of time that will be give before question is repeated
	public float questionRepeatTime = 10f;

	// Canvas object holder where we will parent the buttons to
	public GameObject currentCanvas;

	private CountQuestion currentQuestion;
	private float questionTimer = 0f;
	
	// Variables that will hold fixed sounds for  all questions
	public AudioClip bellSound;
	public AudioClip buzzerSound;

	// Use this for initialization
	void Start () {

		// Get the first question
		currentQuestion = questions[0];

	}

	/// <summary>
	/// Register event handlers
	/// </summary>
	void OnEnable(){
		QuestionEventManager.Correct += OnCorrect;
		QuestionEventManager.Wrong += OnWrong;
		QuestionEventManager.GameStart += OnStart;
	}

	/// <summary>
	/// Clean up event handlers.
	/// </summary>
	void OnDisable(){
		QuestionEventManager.Correct -= OnCorrect;
		QuestionEventManager.Wrong -= OnWrong;
		QuestionEventManager.GameStart -= OnStart;
	}


	// Update is called once per frame
	void Update () {
		if (QuestionState.Instance.timerOn){
			questionTimer += Time.deltaTime;

			//We'll play the question again questionRepeatTime seconds after the question plays 
			float currentWaitTime = questionRepeatTime + currentQuestion.questionEngSound.length +
									currentQuestion.questionSpaSound.length;
			if (questionTimer  >= currentWaitTime){
				audio.clip = currentQuestion.questionSpaSound;
				audio.Play();
				StartCoroutine(playAfter(currentQuestion.questionSpaSound, currentQuestion.questionEngSound));
				questionTimer = 0f;
			}
		}

	}

	/// <summary>
	/// Main routine that instantiates all objects in the current question into the scene.
	/// </summary>
	/// <param name="question">Question.</param>
	void CreateQuestion(CountQuestion question){

		// Create Image of items to count 
		Instantiate(question.itemsSprite, itemsLocation.position, Quaternion.identity);

		// Play question audio for the first time
		audio.clip = currentQuestion.questionSpaSound;
		audio.Play();

		// Play the question in english after the one in spanish
		StartCoroutine(playAfter(currentQuestion.questionSpaSound, currentQuestion.questionEngSound));


		// Instantiate answers
		for (int i = 0; i < question.answers.Count; i++){
			Vector3 questionPos = new Vector3(firstAnswerLoc.position.x + i * answerSpacing, firstAnswerLoc.position.y, 
			                                  firstAnswerLoc.position.z);

			GameObject answer = Instantiate(question.answers[i], questionPos, Quaternion.identity) as GameObject;
			answer.transform.parent = currentCanvas.transform;

			// Positioning hack made so UI prefabs are instantiated at the right position and right size
			RectTransform answerRect = answer.GetComponent<RectTransform>();
			answer.transform.localScale = new Vector3(1f, 1f, 1f);
			answerRect.localPosition = questionPos * 100f;
		}

		// Start question timer after a set amount of time
		StartCoroutine(setTimerAfter(currentQuestion.questionSpaSound.length + currentQuestion.questionEngSound.length));
	}


	/// <summary>
	/// Turn on the timer after a set amount of time.
	/// </summary>
	/// <returns>The timer after.</returns>
	/// <param name="time">Time.</param>
	private IEnumerator setTimerAfter(float time){
		yield return new WaitForSeconds(time);
	    QuestionState.Instance.timerOn = true;
		yield break;
	}

	/// <summary>
	/// Plays a clip after a give clip's length in seconds
	/// </summary>
	/// <returns>The after.</returns>
	/// <param name="current">Current clip playing.</param>
	/// <param name="following">Following clip to play.</param>
	private IEnumerator playAfter(AudioClip current, AudioClip following){
		yield return new WaitForSeconds(current.length);
		audio.Stop();
		audio.clip = following;
		audio.Play();
		yield break;
	}

	/// <summary>
	/// Plays the next question after a set amount of time
	/// </summary>
	/// <returns>The question after.</returns>
	/// <param name="time">Time.</param>
	private IEnumerator NextQuestionAfter(float time){
		yield return new WaitForSeconds(time); 
		QuestionState.Instance.NextQuestion();
		if (questions.Count > 1){
			questions.RemoveAt(0);
			CreateQuestion(questions[0]);
		}   else {
			QuestionState.Instance.EndGame();
		}

	}



	/// <summary>
	/// Raises the start event.
	/// </summary>
	void OnStart(){
		CreateQuestion(currentQuestion);
	}


	/// <summary>
	/// Runs triggered by an event when user clicks on the correct answer.
	/// </summary>
	void OnCorrect(){
		QuestionState.Instance.timerOn = false;
		audio.Stop();
		audio.clip = bellSound;
		audio.Play();
		StartCoroutine(playAfter(bellSound, currentQuestion.correctSound));
		StartCoroutine(NextQuestionAfter(bellSound.length + currentQuestion.correctSound.length));
	
	}


	/// <summary>
	/// Runs triggered by an event when user clicks on the wrong answer.
	/// </summary>
	void  OnWrong(){
		QuestionState.Instance.timerOn = false;
		audio.Stop();
		audio.clip = buzzerSound;
		audio.Play();
		StartCoroutine(playAfter(buzzerSound, currentQuestion.wrongSound));

		StartCoroutine(NextQuestionAfter(buzzerSound.length + currentQuestion.wrongSound.length));
	}



}