﻿using UnityEngine;
using System.Collections;

public class CorrectAnswerButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick(){
		if (QuestionState.Instance.timerOn){
			QuestionState.Instance.markCorrect();
		}

	}
}
