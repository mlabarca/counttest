﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class CountQuestion  {
	// A public class to store information for each question and all its attributes.

	// All sounds required to make this question work
	public AudioClip questionEngSound;
	public AudioClip questionSpaSound;
	public AudioClip correctSound;
	public AudioClip wrongSound;

	//Answers list with gameobjects with buttons
	public List<GameObject> answers;

	//Items image to show up top
	public GameObject itemsSprite;




}
