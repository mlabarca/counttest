﻿using UnityEngine;
using System.Collections;

public class DestroybeforeNext : MonoBehaviour {
	// Destroy object no matter what choice is picked


	// Use this for initialization
	void Start () {

	}

	// register handle
	void OnEnable(){
		QuestionEventManager.NextQuestion += OnAnswerSelected;
	}

	void OnDisable(){
		QuestionEventManager.NextQuestion -= OnAnswerSelected;
	}

	
	// Update is called once per frame
	void Update () {
	
	}


	void OnAnswerSelected(){
		Destroy(gameObject);
	}
}
