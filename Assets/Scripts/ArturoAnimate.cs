﻿using UnityEngine;
using System.Collections;

public class ArturoAnimate : MonoBehaviour {

	private Animator anim;

	public float targetXPercentage = 0.9f;
	public float animSpeed = 4.5f;

	private float targetXPosition;
	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator>();

		// Calculate the position in world space which is the screen space divided by 100, and that divided by 2
		targetXPosition = Screen.width * targetXPercentage / 200f;

	}
	
	// Update is called once per frame
	void Update () {

	
	}

	void OnEnable(){
		QuestionEventManager.GameOver += OnGameOver;
	}

	void OnDisable(){
		QuestionEventManager.GameOver -= OnGameOver;
	}

	void OnGameOver(){
		anim.SetTrigger("Turn");
		StartCoroutine(Translate());
	}

	private IEnumerator Translate(){

		while(gameObject.transform.position.x < targetXPosition){
				Vector3 position = gameObject.transform.position;
				gameObject.transform.position = new Vector3(position.x + Time.deltaTime * animSpeed, position.y, position.z);
				yield return null;
		}

		anim.SetBool("AtTarget", true);

		yield break;
	}



}
