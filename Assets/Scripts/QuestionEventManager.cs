﻿using UnityEngine;
using System.Collections;

public static class QuestionEventManager {
	// Class that registers all events and finally calls them if triggered.
	// Will not call events if no methods have been registered to them.

	public delegate void GameEvent();
	
	public static event GameEvent GameStart, GameOver, Wrong, Correct, NextQuestion;

	public static void TriggerGameStart(){
		if(GameStart != null){
			GameStart();
		}
	}
	
	public static void TriggerGameOver(){
		if(GameOver != null){
			GameOver();
		}
	}
	
	public static void TriggerWrong(){
		if (Wrong != null){
			Wrong();
		}
	}

	public static void TriggerCorrect(){
		if (Correct != null){
			Correct();
		}
	}

	public static void TriggerNext(){
		if (NextQuestion != null){
			NextQuestion();
		}
	}

}
